const categories = [
  {
    value: "strings",
    name: "Virtnes",
    subCategories: [{ value: "small", name: "Mazās" }]
  },
  {
    value: "tin",
    name: "Folija baloni",
    subCategories: [{ value: "bubble", name: "Bubble" }]
  },
  {
    value: "latex",
    name: "Lateksa baloni",
    subCategories: [
      { value: "15cm", name: "15cm" },
      { value: "30cm", name: "30cm" },
      { value: "45cm", name: "45cm" }
    ]
  },
  {
    value: "wrappings",
    name: "Dāvanu iesaiņošanas materiāli",
    subCategories: []
  },
  {
    value: "accessories",
    name: "Svētku aksesuāri",
    subCategories: []
  },
  {
    value: "candles",
    name: "Svecītes",
    subCategories: []
  },
  {
    value: "table",
    name: "Galda piederumi",
    subCategories: []
  },
  {
    value: "cards",
    name: "Apsveikuma kartiņas",
    subCategories: []
  }
];

export default categories;
