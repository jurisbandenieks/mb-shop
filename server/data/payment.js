const paymentMethods = [
  {
    title: "Skaidrā naudā",
    value: "cash"
  },
  {
    title: "Ar karti",
    value: "stripe"
  }
];

export default paymentMethods;
