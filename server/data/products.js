const products = [
  {
    name: "Testa lateksa balons",
    productId: "1",
    image: ["/images/lateksa.jpg"],
    description:
      " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ex delectus temporibus sequi modi ratione quam quas consequuntur incidunt quibusdam odit cum, quia distinctio fugiat culpa maiores, pariatur corporis officia, magni exercitationem. Exercitationem eius adipisci quam amet praesentium necessitatibus, quibusdam repudiandae. Inventore, soluta. Facilis necessitatibus non voluptatibus error mollitia. Officia, expedita!",
    category: { value: "15cm", name: "15cm" },
    price: 0.5,
    countInStock: 100,
    canInflate: true,
    weightPrice: 0.99,
    heliumPrice: 0.49
  },
  {
    name: "Testa folija balons",
    productId: "2",
    image: ["/images/tinfoil.jpg"],
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum, voluptatibus natus? Impedit et id quos consectetur illo fugit tempore ducimus, libero iusto, accusantium ullam excepturi voluptates itaque? Iure, delectus laborum.",
    category: { value: "bubble", name: "Bubble" },
    price: 1.5,
    countInStock: 200
  },
  {
    name: "Testa lateksa balons 2",
    productId: "3",
    image: ["/images/lateks2.jpg"],
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusamus, pariatur?",
    category: { value: "30cm", name: "30cm" },
    price: 0.19,
    countInStock: 500
  },
  {
    name: 'Folija "0" balons, sudraba',
    productId: "4",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons2",
    productId: "5",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons3",
    productId: "6",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons4",
    productId: "7",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons5",
    productId: "8",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons6",
    productId: "9",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons7",
    productId: "10",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons8",
    productId: "11",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons9",
    productId: "12",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  },
  {
    name: "Testa bubble balons10",
    productId: "13",
    image: ["/images/bubble.jpg"],
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt quas natus illum! Beatae at eligendi harum perspiciatis exercitationem omnis tenetur quam. Libero provident ipsum, harum accusantium placeat tenetur nisi sit?",
    category: { name: "Bubble", value: "bubble" },
    price: 9.99,
    countInStock: 0
  }
];

export default products;
