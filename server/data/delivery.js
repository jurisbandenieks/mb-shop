const deliveryMethods = [
  {
    title: "Saņemt veikalā",
    value: "inshop",
    canPayByCash: true
  },
  {
    title: "Piegāde uz mājām",
    value: "homedelivery",
    canPayByCash: true
  }
];

export default deliveryMethods;
