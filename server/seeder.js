import mongoose from "mongoose";
import dotenv from "dotenv";
import colors from "colors";

import categories from "./data/categories.js";
import products from "./data/products.js";
import paymentMethods from "./data/payment.js";
import deliveryMethods from "./data/delivery.js";

import Category from "./models/categoryModel.js";
import Product from "./models/productModel.js";
import Order from "./models/orderModel.js";
import PaymentMethod from "./models/paymentModel.js";
import DeliveryMethod from "./models/deliveryModel.js";

import connectDB from "./config/db.js";

dotenv.config();

connectDB();

const importData = async () => {
  try {
    await Order.deleteMany();
    await Product.deleteMany();
    await Category.deleteMany();
    await PaymentMethod.deleteMany();
    await DeliveryMethod.deleteMany();

    await Category.insertMany(categories);
    await Product.insertMany(products);
    await PaymentMethod.insertMany(paymentMethods);
    await DeliveryMethod.insertMany(deliveryMethods);

    console.log("Data imported!".green.inverse);
    process.exit();
  } catch (error) {
    console.error(`${error}`.red.inverse);
    process.exit(1);
  }
};

const destroyData = async () => {
  try {
    await Order.deleteMany();
    await Product.deleteMany();
    await Category.deleteMany();

    console.log("Data destroyed!".red.inverse);
    process.exit();
  } catch (error) {
    console.error(`${error}`.red.inverse);
    process.exit(1);
  }
};

if (process.argv[2] === "-d") {
  destroyData();
} else {
  importData();
}
