import express from "express";
import { getDeliveryMethods } from "../controllers/deliveryController.js";

const router = express.Router();
router.route("/").get(getDeliveryMethods);

export default router;
