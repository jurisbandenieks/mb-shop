import express from "express";
import bodyParser from "body-parser";
import { getPaymentMethods } from "../controllers/paymentController.js";
import {
  createCheckoutSession,
  stripeWebhooks
} from "../controllers/stripeController.js";
import { paypalSession } from "../controllers/paypalController.js";

const router = express.Router();

router
  .route("/stripe-webhooks")
  .post(bodyParser.raw({ type: "application/json" }), stripeWebhooks);
router.route("/pay-stripe").post(bodyParser.json(), createCheckoutSession);

router.route("/").get(getPaymentMethods);

router.route("/paypal").get(paypalSession);

export default router;
