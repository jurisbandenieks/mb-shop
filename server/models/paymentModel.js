import mongoose from "mongoose";

const paymentMethodSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    value: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

const PaymentMethod = mongoose.model("PaymentMethod", paymentMethodSchema);

export default PaymentMethod;
