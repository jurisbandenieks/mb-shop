import mongoose from "mongoose";

const deliveryMethodSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    value: {
      type: String,
      required: true
    },
    canPayByCash: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
);

const DeliveryMethod = mongoose.model("DeliveryMethod", deliveryMethodSchema);

export default DeliveryMethod;
