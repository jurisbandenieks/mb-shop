import mongoose from "mongoose";

const subCategorySchema = mongoose.Schema({
  value: {
    type: String,
    default: ""
  },
  name: {
    type: String,
    default: ""
  }
});

const categorySchema = mongoose.Schema(
  {
    value: {
      type: String,
      default: ""
    },
    name: {
      type: String,
      default: ""
    },
    subCategories: [subCategorySchema]
  },
  {
    timestamps: true
  }
);

const Category = mongoose.model("Category", categorySchema);

export default Category;
