import mongoose from "mongoose";

const categorySchema = mongoose.Schema({
  name: {
    type: String,
    default: ""
  },
  value: {
    type: String,
    default: ""
  }
});

const productSchema = mongoose.Schema(
  {
    productId: {
      type: String,
      required: true,
      unique: true
    },
    canInflate: {
      type: Boolean,
      default: true
    },
    name: {
      type: String,
      required: true,
      unique: true
    },
    image: [{ type: String, required: true }],
    category: categorySchema,
    weightPrice: {
      type: Number,
      default: 0
    },
    heliumPrice: {
      type: Number,
      default: 0
    },
    description: {
      type: String,
      default: ""
    },
    price: {
      type: Number,
      required: true,
      default: 0
    },
    countInStock: {
      type: Number,
      required: true,
      default: 0
    },
    canDeliver: {
      type: Boolean,
      required: true,
      default: true
    }
  },
  {
    timestamps: true
  }
);

const Product = mongoose.model("Product", productSchema);

export default Product;
