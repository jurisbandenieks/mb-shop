import nodemailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport";
import dotenv from "dotenv";
dotenv.config();

const transport = nodemailer.createTransport(
  smtpTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
      user: process.env.EMAIL,
      pass: process.env.EMAIL_PASS
    }
  })
);

const sendNotificationToAdmin = (order) => {
  const message = {
    from: process.env.EMAIL,
    to: process.env.EMAIL,
    subject: `Receipt nr.${order._id}`,
    html: `<h1>Saņemts jauns pasūtījums
    <a href=${"https://shop.martinaballite.lv/" + order._id}>${
      order._id
    }</a></h1>`
  };

  send(message);
};

const sendMailToCostumer = (order) => {
  const message = {
    from: process.env.EMAIL,
    to: order.shippingAddress.email,
    subject: `Order nr.${order._id}`,
    html: `
    <h1>Paldies par pasūtījumu!</h1>
    <p>Jūsu pasūtijuma nr. <a href=${
      "https://shop.martinaballite.lv/" + order._id
    }>${order._id}</a></p>`
  };

  send(message);
};

const send = (msg) => {
  transport.sendMail(msg, (err, info) => {
    if (err) {
      console.log(err);
    } else {
      console.log(info);
    }
  });
};

export { sendMailToCostumer, sendNotificationToAdmin };
