import asyncHandler from "express-async-handler";
import PaymentMethod from "../models/paymentModel.js";

// @desc    Fetch all categories
// @route   GET /api/payments
// @access  Public route
const getPaymentMethods = asyncHandler(async (req, res) => {
  const paymentMethods = await PaymentMethod.find();

  res.json(paymentMethods);
});

export { getPaymentMethods };
