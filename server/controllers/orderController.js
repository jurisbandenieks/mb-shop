import asyncHandler from "express-async-handler";
import Order from "../models/orderModel.js";
import Product from "../models/productModel.js";
import {
  sendMailToCostumer,
  sendNotificationToAdmin
} from "../functions/sendEmail.js";

// @desc    Create new order
// @route   POST /api/orders
// @access  Private route
const addOrderItems = asyncHandler(async (req, res) => {
  const { orderItems, shippingAddress, paymentMethod } = req.body;

  if (orderItems && orderItems.length === 0) {
    res.status(400);
    throw new Error("No order items");
  } else {
    let products = [];
    let itemsPrice = 0;

    await Promise.all(
      orderItems.map(async (item) => {
        const productItem = await Product.findById(item.product);

        const itemHeliumPrice = item.helium ? productItem.heliumPrice : 0;
        const itemWeightPrice = item.weight ? productItem.weightPrice : 0;
        const itemTotalPrice =
          (productItem.price + itemHeliumPrice) * item.qty + itemWeightPrice;

        products.push({
          qty: item.qty,
          name: productItem.name,
          image: productItem.image,
          price: productItem.price,
          heliumPrice: itemHeliumPrice,
          weightPrice: itemWeightPrice,
          totalPrice: itemTotalPrice,
          product: productItem
        });

        itemsPrice += itemTotalPrice;
      })
    );

    const shippingPrice =
      itemsPrice > 20 || shippingAddress.deliveryMethod === "inshop" ? 0 : 5;
    const totalPrice = itemsPrice + shippingPrice;
    const taxPrice = totalPrice * 0.21;

    const order = new Order({
      orderItems: products,
      shippingAddress,
      paymentMethod,
      itemsPrice: Number(itemsPrice.toFixed(2)),
      taxPrice: Number(taxPrice.toFixed(2)),
      shippingPrice: Number(shippingPrice.toFixed(2)),
      totalPrice: Number(totalPrice.toFixed(2))
    });

    const createdOrder = await order.save();

    sendNotificationToAdmin(order);
    sendMailToCostumer(order);

    res.status(201).json(createdOrder);
  }
});

// @desc    Get order by id
// @route   GET /api/orders/:id
// @access  Private route
const getOrderById = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id).populate(
    "user",
    "name email"
  );

  if (order) {
    res.json(order);
  } else {
    res.status(404);
    throw new Error("Order not found");
  }
});

// @desc    Update order to paid
// @route   PUT /api/orders/pay
// @access  Private route
const updateOrderToPaid = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (order) {
    if (order.orderItems && order.orderItems.length > 0) {
      order.orderItems.forEach(async (item) => {
        const product = await Product.findById(item.product);

        if (product) {
          await product.updateOne({
            countInStock: product.countInStock - item.qty
          });
        } else {
          res.status(404);
          throw new Error("Product not found");
        }
      });
    }

    order.isPaid = true;
    order.paidAt = Date.now();

    const updatedOrder = await order.save();

    res.json(updatedOrder);
  } else {
    res.status(404);
    throw new Error("Order not found");
  }
});

// @desc    Update order to delivered
// @route   PUT /api/orders/:id/deliver
// @access  Private/Admin route
const updateOrderToDelivered = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (order) {
    order.isDelivered = true;
    order.deliveredAt = Date.now();

    const updatedOrder = await order.save();

    res.json(updatedOrder);
  } else {
    res.status(404);
    throw new Error("Order not found");
  }
});

// @desc    Get all orders
// @route   GET /api/orders/myorders
// @access  Private route
const getOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({}).populate("user", "id name");

  res.json(orders);
});

export {
  addOrderItems,
  getOrderById,
  updateOrderToPaid,
  updateOrderToDelivered,
  getOrders
};
