import Stripe from "stripe";
import dotenv from "dotenv";

import Order from "../models/orderModel.js";
import Product from "../models/productModel.js";
import DeliveryMethod from "../models/deliveryModel.js";

dotenv.config();

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

// @desc    POST payment
// @route   POST /api/payments/pay-stripe
// @access  Public route
const createCheckoutSession = async (req, res) => {
  try {
    const info = {
      callbackUrl: req.body.callbackUrl,
      orderId: req.body.orderId
    };

    const order = await Order.findById(info.orderId);

    const sessionConfig = setupPurchaseCourseSession(info, order);

    const session = await stripe.checkout.sessions.create(sessionConfig);

    res.status(200).json({
      stripeCheckoutSessionId: session.id,
      stripePublicKey: process.env.STRIPE_PUBLIC_KEY
    });
  } catch (error) {
    res.status(500).json({ error: "Could not initiate session" });
  }
};

const setupPurchaseCourseSession = (info, order) => {
  const config = setupBaseSessionConfig(info, order);

  const configItems = [];

  order.orderItems.forEach((product) => {
    configItems.push({
      name: product.name,
      amount: (product.price + product.heliumPrice) * 100,
      currency: "eur",
      quantity: product.qty
    });

    if (product.weightPrice > 0) {
      configItems.push({
        name: `${product.name} - atsvariņi`,
        amount: product.weightPrice * 100,
        currency: "eur",
        quantity: 1
      });
    }
  });

  if (order.shippingAddress.deliveryMethod === "homedelivery") {
    configItems.push({
      name: `Piegāde uz mājām`,
      amount: order.shippingPrice * 100,
      currency: "eur",
      quantity: 1
    });
  }

  return { ...config, line_items: configItems };
};

const setupBaseSessionConfig = (info, order) => {
  const config = {
    customer_email: order.shippingAddress.email,
    payment_method_types: ["card"],
    success_url: `${info.callbackUrl}/${info.orderId}?purchaseResult=success`,
    cancel_url: `${info.callbackUrl}/${info.orderId}?purchaseResult=failed`,
    client_reference_id: info.orderId
  };

  return config;
};

// @desc    POST payment
// @route   POST /api/payments/stripe-webhooks
// @access  Public route

const stripeWebhooks = async (req, res) => {
  try {
    const signature = req.headers["stripe-signature"];

    const event = stripe.webhooks.constructEvent(
      req.body,
      signature,
      process.env.STRIPE_WEBHOOK_SECRET
    );

    if (event.type == "checkout.session.completed") {
      const session = event.data.object;
      onCheckoutSessionCompleted(session);
    }

    res.json({ received: true });
  } catch (err) {
    console.log("Error processing webhook event, reason: ", err);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }
};

const onCheckoutSessionCompleted = async (session) => {
  // Gets the order
  const purchaseSessionId = session.client_reference_id;

  const order = await Order.findById(purchaseSessionId);
  if (order) {
    const items = order.orderItems;

    await Promise.all(
      items.map(async (item) => {
        const product = await Product.findById(item.product);
        if (product) {
          product.countInStock = product.countInStock - item.qty;
          await product.save();
        }
      })
    );

    await updateToPayed(order);
  }
};

const updateToPayed = async (order) => {
  order.isPaid = true;
  order.paidAt = new Date().toISOString();
  await order.save();
};

export { createCheckoutSession, stripeWebhooks };
