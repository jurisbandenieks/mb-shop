import asyncHandler from "express-async-handler";
import DeliveryMethod from "../models/deliveryModel.js";

// @desc    Fetch all categories
// @route   GET /api/categories
// @access  Public route
const getDeliveryMethods = asyncHandler(async (req, res) => {
  const deliveries = await DeliveryMethod.find();

  res.json(deliveries);
});

export { getDeliveryMethods };
