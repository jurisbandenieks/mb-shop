import asyncHandler from "express-async-handler";
import Category from "../models/categoryModel.js";

// @desc    Fetch all categories
// @route   GET /api/categories
// @access  Public route
const getCategories = asyncHandler(async (req, res) => {
  const categories = await Category.find();

  res.json(categories);
});

// @desc    Delete a category
// @route   DELETE /api/categories/:id
// @access  Private/Admin route
const deleteCategory = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (category) {
    await category.remove();
    res.json({ message: "Category deleted successfully" });
  } else {
    res.status(404);
    throw new Error("Category not Found");
  }
});

// @desc    Create a category
// @route   POST /api/categories
// @access  Private/Admin route
const createCategory = asyncHandler(async (req, res) => {
  const category = new Category({
    value: "sample",
    name: "Sample category",
    subCategories: []
  });

  const createdCategory = await category.save();

  res.status(201).json(createdCategory);
});

// @desc    Update a category
// @route   PUT /api/categories/:id
// @access  Private/Admin route
const updateCategory = asyncHandler(async (req, res) => {
  const {
    value: updatedValue,
    name: updatedName,
    subCategories: updatedSubCategories
  } = req.body;

  const category = await Category.findById(req.params.id);

  if (category) {
    const updatedCategory = await category.updateOne({
      value: updatedValue,
      name: updatedName,
      subCategories: updatedSubCategories
    });

    res.json(updatedCategory);
  } else {
    res.status(404);
    throw new Error("Category not Found");
  }
});

export { getCategories, deleteCategory, createCategory, updateCategory };
