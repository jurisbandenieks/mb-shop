import asyncHandler from "express-async-handler";
import Product from "../models/productModel.js";
import Category from "../models/categoryModel.js";

// @desc    Fetch all products
// @route   GET /api/products
// @access  Public route
const getProducts = asyncHandler(async (req, res) => {
  const pageSize = 10;
  const page = Number(req.query.pageNumber) || 1;

  let findCategory;

  findCategory = await Category.findOne({
    "subCategories.value": req.query.keyword
  });

  if (
    findCategory &&
    findCategory.subCategories &&
    findCategory.subCategories.length > 0 &&
    findCategory.value !== req.query.keyword
  ) {
    findCategory = findCategory.subCategories.find(
      (child) => child.value === req.query.keyword
    );
  }

  const cat = req.query.keyword
    ? {
        "category.value": {
          $regex: findCategory.value
        }
      }
    : {};

  const count = await Product.countDocuments({ ...cat });
  const products = await Product.find({ ...cat })
    .sort({ createdAt: "desc" })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  res.json({ products, page, pages: Math.ceil(count / pageSize), pageSize });
});

// @desc    Fetch single product
// @route   GET /api/products/:id
// @access  Public route
const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.json(product);
  } else {
    res.status(404);
    throw new Error("Product not Found");
  }
});

// @desc    Delete a product
// @route   DELETE /api/products/:id
// @access  Private/Admin route
const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    await product.remove();
    res.json({ message: "Product deleted successfully" });
  } else {
    res.status(404);
    throw new Error("Product not Found");
  }
});

// @desc    Create a product
// @route   POST /api/products
// @access  Private/Admin route
const createProduct = asyncHandler(async (req, res) => {
  const product = new Product({
    name: "Sample name",
    productId: "0",
    price: 0,
    image: ["/images/sample.png"],
    category: { name: "", value: "" },
    countInStock: "0",
    canDeliver: true,
    description: "Sample description"
  });

  const createdProduct = await product.save();

  res.status(201).json(createdProduct);
});

// @desc    Update a product
// @route   PUT /api/products/:id
// @access  Private/Admin route
const updateProduct = asyncHandler(async (req, res) => {
  const {
    name,
    productId,
    price,
    description,
    image,
    category,
    countInStock,
    canInflate,
    heliumPrice,
    weightPrice,
    canDeliver
  } = req.body;

  const product = await Product.findById(req.params.id);

  if (product) {
    product.name = name;
    product.productId = productId;
    product.price = price;
    product.description = description;
    product.image = image;
    product.category = category;
    product.canDeliver = canDeliver;
    product.canInflate = canInflate;
    product.heliumPrice = heliumPrice;
    product.weightPrice = weightPrice;
    product.countInStock = countInStock;

    const updatedProduct = await product.save();

    res.json(updatedProduct);
  } else {
    res.status(404);
    throw new Error("Product not Found");
  }
});

export {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct
};
