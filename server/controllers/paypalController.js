import asyncHandler from "express-async-handler";

// @desc    Fetch all categories
// @route   GET /api/categories
// @access  Public route
const paypalSession = asyncHandler(async (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID);
});

export { paypalSession };
