import express from "express";
import dotenv from "dotenv";
import colors from "colors";
import morgan from "morgan";
import connectDB from "./config/db.js";

import { notFound, errorHandler } from "./middleware/errorMiddleware.js";

import categoryRoutes from "./routes/categoryRoutes.js";
import productRoutes from "./routes/productRoutes.js";
import userRoutes from "./routes/userRoutes.js";
import orderRoutes from "./routes/orderRoutes.js";
import uploadRoutes from "./routes/uploadRoutes.js";
import paymentRoutes from "./routes/paymentRoutes.js";
import deliveryRoutes from "./routes/deliveryRoutes.js";

dotenv.config();
connectDB();

const app = express();
const port = process.env.PORT || 5000;

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use("/api/payments", paymentRoutes);
app.use("/api/categories", express.json(), categoryRoutes);
app.use("/api/products", express.json(), productRoutes);
app.use("/api/users", express.json(), userRoutes);
app.use("/api/orders", express.json(), orderRoutes);
app.use("/api/upload", express.json(), uploadRoutes);
app.use("/api/deliveries", express.json(), deliveryRoutes);

app.get("/api", (req, res) => {
  res.send("API is running....");
});

app.use(notFound);
app.use(errorHandler);

app.listen(
  port,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${port}`.yellow.bold
  )
);
