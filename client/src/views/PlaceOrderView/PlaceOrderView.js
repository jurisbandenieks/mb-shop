import React, { useEffect } from "react";
import "./PlaceOrderView.scss";

import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createOrder } from "../../actions/orderActions";
import {
  Grid,
  List,
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemText,
  Button,
  Divider
} from "@material-ui/core";

import Message from "../../components/Message/Message";

const PlaceOrderView = ({ backToShipping, history }) => {
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);

  const addDecimals = (num) => {
    return (Math.round(num * 100) / 100).toFixed(2);
  };

  cart.heliumPrice = addDecimals(
    cart.cartItems.reduce((acc, item) => {
      const heliumPrice = item.helium ? Number(item.heliumPrice) : 0;
      return acc + heliumPrice * item.qty;
    }, 0)
  );

  cart.weightPrice = addDecimals(
    cart.cartItems.reduce((acc, item) => {
      const weightPrice = item.weight ? Number(item.weightPrice) : 0;
      return acc + weightPrice;
    }, 0)
  );

  cart.itemsPrice = addDecimals(
    cart.cartItems.reduce((acc, item) => {
      return acc + item.price * item.qty;
    }, 0)
  );

  cart.shippingPrice = addDecimals(
    cart.itemsPrice > 20 || cart.shippingAddress.deliveryMethod === "inshop"
      ? 0
      : 5
  );

  cart.totalPrice = addDecimals(
    Number(cart.itemsPrice) +
      Number(cart.shippingPrice) +
      Number(cart.heliumPrice) +
      Number(cart.weightPrice)
  );

  cart.taxPrice = addDecimals(0.21 * Number(cart.totalPrice));
  cart.netoPrice = addDecimals(Number(cart.totalPrice) - Number(cart.taxPrice));

  const orderCreate = useSelector((state) => state.orderCreate);
  const { order, success, error } = orderCreate;

  useEffect(() => {
    if (success) {
      history.push(`/order/${order._id}`);
    }
    // eslint-disable-next-line
  }, [history, success]);

  const placeOrderHandler = () => {
    dispatch(
      createOrder({
        orderItems: cart.cartItems,
        shippingAddress: cart.shippingAddress,
        paymentMethod: cart.paymentMethod
      })
    );
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={8}>
        <List dense>
          <ListItem>
            <ListItemText
              primary="Adrese"
              secondary={`${cart.shippingAddress.address}, ${cart.shippingAddress.city},
                    LV-${cart.shippingAddress.postalCode}`}
            />
          </ListItem>

          <ListItem>
            <ListItemText
              primary="Norēķina veids"
              secondary={cart.paymentMethod.title}
            />
          </ListItem>

          <ListItem>
            <ListItemText
              primary="Komentāri"
              secondary={cart.shippingAddress.comments}
            />
          </ListItem>

          <ListItem>
            {cart.cartItems.length === 0 ? (
              <ListItemText
                primary="Pasūtijuma grozs"
                secondary="Jūsu grozs ir tukšs"
              />
            ) : (
              <>
                <ListItemText primary="Pasūtijuma grozs" />
                <List dense variant="flush">
                  {cart.cartItems.map((item, index) => (
                    <ListItem key={index}>
                      <ListItemAvatar>
                        <Avatar alt={item.name} src={item.image[0]} />
                      </ListItemAvatar>

                      <Link to={`product/${item.product}`}>{item.name}</Link>

                      <ListItemText
                        primary={`${item.qty} x €${item.price} = €${
                          item.qty * item.price
                        }`}
                      />
                    </ListItem>
                  ))}
                </List>
              </>
            )}
          </ListItem>
        </List>
      </Grid>
      <Grid item>
        <List variant="flush">
          <ListItem>
            <h2>Pasūtījuma kopsavilkums</h2>
          </ListItem>
          <Divider />
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                Preces
              </Grid>
              <Grid item>€{cart.itemsPrice}</Grid>
            </Grid>
          </ListItem>

          {Number(cart.heliumPrice) + Number(cart.weightPrice) > 0 && (
            <ListItem>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  Papildus
                </Grid>
                <Grid item>
                  €
                  {(
                    Number(cart.heliumPrice) + Number(cart.weightPrice)
                  ).toFixed(2)}
                </Grid>
              </Grid>
            </ListItem>
          )}
          <Divider />
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                Piegāde
              </Grid>
              <Grid item>€{cart.shippingPrice}</Grid>
            </Grid>
          </ListItem>
          <Divider />
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                PVN
              </Grid>
              <Grid item>€{cart.taxPrice}</Grid>
            </Grid>
          </ListItem>
          <Divider />
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                Pirms PVN
              </Grid>
              <Grid item>€{cart.netoPrice}</Grid>
            </Grid>
          </ListItem>
          <Divider />
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                Kopā
              </Grid>
              <Grid item>€{cart.totalPrice}</Grid>
            </Grid>
          </ListItem>
          <Divider />
          <ListItem>
            {error && <Message variant="error">{error}</Message>}
          </ListItem>
          <ListItem>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <Button
                  type="button"
                  variant="contained"
                  color="secondary"
                  onClick={backToShipping}
                >
                  Atpakaļ
                </Button>
              </Grid>
              <Grid item xs={12} md={6}>
                <Button
                  type="button"
                  variant="contained"
                  color="primary"
                  disabled={cart.cartItems === 0}
                  onClick={placeOrderHandler}
                >
                  Apstiprināt
                </Button>
              </Grid>
            </Grid>
          </ListItem>
        </List>
      </Grid>
    </Grid>
  );
};

export default PlaceOrderView;
