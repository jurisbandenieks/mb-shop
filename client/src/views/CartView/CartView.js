import React, { useEffect, useState } from "react";
import "./CartView.scss";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addToCart, removeFromCart } from "../../actions/cartActions";
import Message from "../../components/Message/Message";
import Meta from "../../components/Meta";

import {
  ListItemText,
  List,
  ListItem,
  Grid,
  Avatar,
  ListItemAvatar,
  ListItemSecondaryAction,
  IconButton,
  FormControl,
  Select,
  InputLabel,
  MenuItem,
  Button,
  Divider,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const getWindowDimensions = () => {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
};

const CartView = ({ productId, qty, helium, weight, nextToPayment }) => {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (productId) {
      dispatch(addToCart(productId, qty, helium, weight));
    }
  }, [dispatch, productId, qty, helium, weight]);

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };

  return (
    <div className="basket">
      <Meta title="Grozs" />
      <h1>Grozs</h1>
      <p>
        Pasūtījumiem uz mājām kuriem kopējā summa ir zem 20€ tiek piemērota
        papildus 5€ piemaksa
      </p>
      {cartItems.length === 0 ? (
        <Message>
          Your cart is empty <Link to="/">Go Back</Link>
        </Message>
      ) : (
        <Grid container spacing={5}>
          <Grid item xs={12} md={8}>
            <List dense={true}>
              {cartItems.map((item) => (
                <ListItem key={item.product} divider>
                  {windowDimensions.width > 600 && (
                    <Link to={`/products/${item.product}`}>
                      <ListItemAvatar>
                        <Avatar alt={item.name} src={item.image[0]} />
                      </ListItemAvatar>
                    </Link>
                  )}

                  <ListItemText
                    primary={item.name}
                    secondary={item.qty ? `Daudzums: ${item.qty}` : null}
                  />

                  <div className="basket__inputs">
                    {item.canInflate && (
                      <div className="basket__checkboxes">
                        {item.heliumPrice > 0 && (
                          <FormControlLabel
                            control={
                              <Checkbox
                                size="small"
                                color="primary"
                                onChange={(e) =>
                                  dispatch(
                                    addToCart(
                                      item.product,
                                      Number(item.qty),
                                      e.target.checked,
                                      item.weight
                                    )
                                  )
                                }
                              />
                            }
                            checked={item.helium}
                            readOnly
                            label={
                              <span style={{ fontSize: "12px" }}>
                                {"Hēlijs €" + item.heliumPrice.toFixed(2)}
                              </span>
                            }
                            labelPlacement="end"
                          />
                        )}

                        {item.weightPrice > 0 && (
                          <FormControlLabel
                            control={
                              <Checkbox
                                size="small"
                                color="primary"
                                onChange={(e) =>
                                  dispatch(
                                    addToCart(
                                      item.product,
                                      Number(item.qty),
                                      item.helium,
                                      e.target.checked
                                    )
                                  )
                                }
                              />
                            }
                            checked={item.weight}
                            readOnly
                            label={
                              <span style={{ fontSize: "12px" }}>
                                {"Atsvari €" + item.weightPrice.toFixed(2)}
                              </span>
                            }
                            labelPlacement="end"
                          />
                        )}
                      </div>
                    )}
                    {item.countInStock > 0 ? (
                      <FormControl>
                        <InputLabel id="qty-label">Gab.</InputLabel>
                        <Select
                          labelId="qty-label"
                          id="qty"
                          value={item.qty}
                          onChange={(e) =>
                            dispatch(
                              addToCart(
                                item.product,
                                Number(e.target.value),
                                item.needHelium,
                                item.needWeight
                              )
                            )
                          }
                        >
                          {[...Array(item.countInStock).keys()].map((x) => (
                            <MenuItem key={x + 1} value={x + 1}>
                              {x + 1}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    ) : (
                      <span>Nav pieejams</span>
                    )}
                  </div>

                  <ListItemSecondaryAction>
                    <IconButton
                      edge="end"
                      aria-label="delete"
                      onClick={() => removeFromCartHandler(item.product)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </Grid>
          <Grid item md xs={12}>
            <List variant="flush">
              <ListItem>
                <ListItemText
                  primary={`Kopā (${cartItems.reduce(
                    (acc, item) => acc + item.qty,
                    0
                  )}) preces`}
                  secondary={`€${cartItems
                    .reduce((acc, item) => {
                      const heliumTotal = item.helium ? item.heliumPrice : 0;
                      const weightTotal = item.weight ? item.weightPrice : 0;
                      return (
                        acc +
                        (heliumTotal + item.price) * item.qty +
                        weightTotal
                      );
                    }, 0)
                    .toFixed(2)}`}
                />
              </ListItem>
              <Divider />
              <ListItem>
                <Button
                  type="button"
                  className="btn-block"
                  disabled={cartItems.length === 0}
                  onClick={nextToPayment}
                  variant="contained"
                  color="primary"
                >
                  Tālāk
                </Button>
              </ListItem>
            </List>
          </Grid>
        </Grid>
      )}
    </div>
  );
};

export default CartView;
