import React, { useState } from "react";
import { Route } from "react-router-dom";
import { Step, StepLabel, Stepper } from "@material-ui/core";
import CartView from "../CartView/CartView";
import PaymentMethodView from "../PaymentMethodView/PaymentMethodView";
import ShippingView from "../ShippingView/ShippingView";
import PlaceOrderView from "../PlaceOrderView/PlaceOrderView";

const OrderStepsView = ({ match, location }) => {
  const productId = match.params.id;

  const qty = location.search
    ? Number(location.search.split("?")[1].split("=").pop())
    : 1;
  const helium =
    location.search && Number(location.search.split("?")[3].split("=").pop())
      ? true
      : false;
  const weight =
    location.search && Number(location.search.split("?")[3].split("=").pop())
      ? true
      : false;

  const [activeStep, setActiveStep] = useState(0);

  const steps = ["Grozs", "Maksājuma veids", "Piegāde", "Maksājuma izpilde"];

  const nextStep = (s) => {
    setActiveStep(s);
  };

  const previousStep = (s) => {
    setActiveStep(s);
  };

  return (
    <div>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      {activeStep === 0 && (
        <CartView
          productId={productId}
          qty={qty}
          weight={weight}
          helium={helium}
          nextToPayment={() => nextStep(1)}
        />
      )}
      {activeStep === 1 && (
        <PaymentMethodView
          nextToShipping={() => nextStep(2)}
          backToCart={() => previousStep(0)}
        />
      )}
      {activeStep === 2 && (
        <ShippingView
          nextToProcess={() => nextStep(3)}
          backToPayment={() => previousStep(1)}
        />
      )}
      {activeStep === 3 && (
        <Route
          render={({ history }) => (
            <PlaceOrderView
              history={history}
              backToShipping={() => previousStep(2)}
            />
          )}
        />
      )}
    </div>
  );
};

export default OrderStepsView;
