import {
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup
} from "@material-ui/core";
import "./PaymentMethodView.scss";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { savePaymentMethod } from "../../actions/cartActions";

const PaymentMethodView = ({ nextToShipping, backToCart }) => {
  const methods = [
    { title: "Skaidrā naudā", value: "cash" },
    { title: "Ar karti", value: "stripe" }
  ];
  const [paymentMethod, setPaymentMethod] = useState({
    title: "Skaidrā naudā",
    value: "cash"
  });

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(savePaymentMethod(paymentMethod));
    nextToShipping();
  };

  const changePaymentMethods = (param) => {
    const method = methods.find((m) => m.value === param);
    setPaymentMethod(method);
  };
  return (
    <form
      className="payment"
      noValidate
      autoComplete="off"
      onSubmit={submitHandler}
    >
      <FormControl>
        <FormLabel>Maksājuma veids</FormLabel>
        <RadioGroup
          aria-label="gender"
          name={paymentMethod.title}
          value={paymentMethod.value}
          onChange={(e) => changePaymentMethods(e.target.value)}
        >
          {methods.map((m, i) => (
            <FormControlLabel
              key={i}
              control={<Radio />}
              value={m.value}
              label={m.title}
            />
          ))}
        </RadioGroup>
      </FormControl>

      <Button
        variant="contained"
        color="secondary"
        type="button"
        onClick={backToCart}
      >
        Atpakaļ
      </Button>
      <Button variant="contained" color="primary" type="submit">
        Tālāk
      </Button>
    </form>
  );
};

export default PaymentMethodView;
