import React, { useState, useEffect } from "react";
import "./ShippingView.scss";
import { useDispatch, useSelector } from "react-redux";
import { saveShippingAddress } from "../../actions/cartActions";
import {
  TextField,
  Button,
  Checkbox,
  Dialog,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel
} from "@material-ui/core";
import { listDeliveryMethods } from "../../actions/deliveryActions";

import PrivacyContent from "../../components/PrivacyContent/PrivacyContent";

const ShippingView = ({ nextToProcess, backToPayment }) => {
  const [agree, setAgree] = useState(false);
  const [open, setOpen] = useState(false);

  const cart = useSelector((state) => state.cart);
  const { shippingAddress } = cart;

  const [fullName, setFullName] = useState(shippingAddress.fullName || "");
  const [phone, setPhone] = useState(shippingAddress.phone || "");
  const [email, setEmail] = useState(shippingAddress.email || "");
  const [address, setAddress] = useState(shippingAddress.address || "");
  const [city, setCity] = useState(shippingAddress.city || "");
  const [postalCode, setPostalCode] = useState(
    shippingAddress.postalCode || ""
  );
  const [comments, setComments] = useState(shippingAddress.comments || "");

  const [deliveryMethod, setDeliveryMethod] = useState(
    shippingAddress.deliveryMethod || "homedelivery"
  );

  const dispatch = useDispatch();

  const deliveryList = useSelector((state) => state.deliveryList);
  const { deliveryMethods } = deliveryList;

  useEffect(() => {
    dispatch(listDeliveryMethods());
  }, [dispatch]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      saveShippingAddress({
        fullName,
        phone,
        email,
        address,
        city,
        postalCode,
        comments,
        deliveryMethod
      })
    );
    nextToProcess();
  };

  const handleAgree = () => {
    setOpen(false);
    setAgree(true);
  };

  const handleDisagree = () => {
    setOpen(false);
    setAgree(false);
  };

  const handleDeliveryMethodChange = (params) => {
    setDeliveryMethod(params);
    if (params === "inshop") {
      setAddress("T/C Aleja");
      setCity("Rīga");
      setPostalCode("1058");
    } else {
      setAddress("");
      setCity("");
      setPostalCode("");
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="shipping">
        <div>
          <TextField
            label="Vārds, Uzvārds"
            variant="outlined"
            type="text"
            value={fullName}
            required
            onChange={(e) => setFullName(e.target.value)}
          />

          <TextField
            label="Telefona nr."
            variant="outlined"
            type="text"
            value={phone}
            required
            onChange={(e) => setPhone(e.target.value)}
          />

          <TextField
            label="Epasts"
            variant="outlined"
            type="email"
            value={email}
            required
            onChange={(e) => setEmail(e.target.value)}
          />
          {deliveryMethods && deliveryMethods.length > 0 && (
            <FormControl>
              <FormLabel>Piegādes veidi</FormLabel>
              <RadioGroup
                name="deliveryMethod"
                value={deliveryMethod}
                onChange={(e) => handleDeliveryMethodChange(e.target.value)}
                className="flex-start"
              >
                {deliveryMethods.map((m) => (
                  <FormControlLabel
                    key={m._id}
                    value={m.value}
                    control={<Radio />}
                    label={m.title}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          )}

          {deliveryMethod.value !== "inshop" && (
            <>
              <p>Piegādes pagaidām Rīgas/Jelgava apkārtnē</p>
              <div className="col">
                <TextField
                  label="Adrese"
                  variant="outlined"
                  type="text"
                  value={address}
                  required
                  onChange={(e) => setAddress(e.target.value)}
                />

                <TextField
                  label="Pilsēta"
                  variant="outlined"
                  type="text"
                  value={city}
                  required
                  onChange={(e) => setCity(e.target.value)}
                />

                <TextField
                  label="Pasta Indeks"
                  variant="outlined"
                  type="text"
                  value={postalCode}
                  required
                  onChange={(e) => setPostalCode(e.target.value)}
                />
              </div>
            </>
          )}
        </div>
        <div>
          <TextField
            label="Komentāri"
            value={comments}
            variant="outlined"
            helperText="Varat norādīt piezīmes par piegādi un tās laiku"
            onChange={(e) => setComments(e.target.value)}
            multiline
            rows={9}
          />
          <div>
            <Checkbox
              checked={agree}
              color="primary"
              onChange={() => setAgree(!agree)}
            />
            <span>
              Piekrītu{" "}
              <span onClick={() => setOpen(true)} className="shipping__privacy">
                privātuma politikai
              </span>{" "}
              par datu glabāšanu
            </span>
          </div>
        </div>
      </div>
      <div className="shipping__actions flex-justified">
        <Button
          variant="contained"
          color="secondary"
          type="button"
          onClick={backToPayment}
        >
          Atpakaļ
        </Button>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          disabled={!agree}
        >
          Tālāk
        </Button>
      </div>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <PrivacyContent
          handleAgree={handleAgree}
          handleDisagree={handleDisagree}
        />
      </Dialog>
    </form>
  );
};

export default ShippingView;
