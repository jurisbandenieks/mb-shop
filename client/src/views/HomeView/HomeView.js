import React, { useEffect } from "react";
import { Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import "./HomeView.scss";

import { listProducts } from "../../actions/productActions";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import Product from "../../components/Product/Product";
import Meta from "../../components/Meta";
import { Grid } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

const HomeView = ({ match, history }) => {
  const keyword = match.params.keyword;
  const pageNumber = match.params.pageNumber || 1;

  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products, pages, page } = productList;

  useEffect(() => {
    dispatch(listProducts(keyword, pageNumber));
  }, [dispatch, keyword, pageNumber]);

  const renderProduct = (p) => {
    return (
      <Grid item className="home__card" key={p._id}>
        <Route
          render={({ history }) => <Product history={history} product={p} />}
        />
      </Grid>
    );
  };

  const handleChange = (event, value) => {
    history.push(
      keyword ? `/search/${keyword}/page/${value}` : `/page/${value}`
    );
  };

  return (
    <div className="home">
      <Meta />
      {error && <Message variant="error">{error}</Message>}
      {loading && <Loader />}
      <Grid container justify="center" spacing={2}>
        {products &&
          products.length > 0 &&
          products.map((product) => renderProduct(product))}
      </Grid>
      <Pagination
        count={pages || 1}
        page={page || 1}
        className="home__pages"
        onChange={handleChange}
      />
    </div>
  );
};

export default HomeView;
