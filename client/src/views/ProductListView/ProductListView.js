import React, { useEffect } from "react";
import "./ProductListView.scss";

import { useDispatch, useSelector } from "react-redux";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import {
  listProducts,
  deleteProduct,
  createProduct
} from "../../actions/productActions";
import { PRODUCT_CREATE_RESET } from "../../constants/productConstants";
import {
  Button,
  Grid,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TableBody
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";

const ProductListView = ({ history, match }) => {
  const pageNumber = match.params.pageNumber || 1;

  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products, pages, page } = productList;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const productDelete = useSelector((state) => state.productDelete);
  const {
    loading: loadingDelete,
    error: errorDelete,
    success: successDelete
  } = productDelete;

  const productCreate = useSelector((state) => state.productCreate);
  const {
    loading: loadingCreate,
    error: errorCreate,
    success: successCreate,
    product: createdProduct
  } = productCreate;

  useEffect(() => {
    dispatch({ type: PRODUCT_CREATE_RESET });

    if (userInfo && !userInfo.name) {
      history.push("/login");
    }

    if (successCreate) {
      history.push(`/admin/product/${createdProduct._id}/edit`);
    } else {
      dispatch(listProducts("", pageNumber));
    }
  }, [
    dispatch,
    history,
    userInfo,
    successDelete,
    successCreate,
    createdProduct,
    pageNumber
  ]);

  const deleteHandler = (id) => {
    if (window.confirm("Are you sure")) {
      dispatch(deleteProduct(id));
    }
  };

  const createProductHandler = () => {
    dispatch(createProduct());
  };

  const handleChange = (event, value) => {
    history.push(`/admin/productlist/${value}`);
  };

  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <h1>Products</h1>
        </Grid>
        <Grid item xs={6}>
          <Button
            color="primary"
            variant="contained"
            onClick={createProductHandler}
          >
            Create Product
          </Button>
        </Grid>
      </Grid>

      {loadingDelete && <Loader />}
      {errorDelete && <Message variant="error">{errorDelete}</Message>}

      {loadingCreate && <Loader />}
      {errorCreate && <Message variant="error">{errorCreate}</Message>}

      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="error">{error}</Message>
      ) : (
        <>
          <TableContainer component={Paper} className="table">
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell align="right">PRODUCT ID</TableCell>
                  <TableCell align="right">NAME</TableCell>
                  <TableCell align="right">PRICE</TableCell>
                  <TableCell align="right">QTY</TableCell>
                  <TableCell align="right">CATEGORY</TableCell>
                  <TableCell align="right">CAN DELIVER?</TableCell>
                  <TableCell align="right"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((product) => (
                  <TableRow key={product._id}>
                    <TableCell component="th" scope="row">
                      {product._id}
                    </TableCell>
                    <TableCell align="right">{product.productId}</TableCell>
                    <TableCell align="right">{product.name}</TableCell>
                    <TableCell align="right">{product.price}</TableCell>
                    <TableCell align="right">{product.countInStock}</TableCell>
                    <TableCell align="right">{product.category.name}</TableCell>
                    <TableCell align="right">
                      {product.canDeliver ? (
                        <i
                          className="fas fa-check"
                          style={{ color: "green" }}
                        ></i>
                      ) : (
                        <i
                          className="fas fa-times"
                          style={{ color: "red" }}
                        ></i>
                      )}
                    </TableCell>
                    <TableCell align="right">
                      <Button
                        component={Link}
                        to={`/admin/product/${product._id}/edit`}
                        color="primary"
                      >
                        EDIT
                      </Button>

                      <Button
                        color="secondary"
                        onClick={() => deleteHandler(product._id)}
                      >
                        DELETE
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>

          <Pagination
            count={pages || 1}
            page={page || 1}
            onChange={handleChange}
            className="home__pages"
          />
        </>
      )}
    </>
  );
};

export default ProductListView;
