import React, { useState, useEffect } from "react";
import "./LoginView.scss";

import { useDispatch, useSelector } from "react-redux";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import { login } from "../../actions/userActions";
import { Button, TextField } from "@material-ui/core";

const LoginView = ({ location, history }) => {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  const redirect = location.search ? location.search.split("=")[1] : "/";

  useEffect(() => {
    if (userInfo) {
      history.push(redirect);
    }
  }, [history, userInfo, redirect]);

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(login(name, password));
  };

  return (
    <div className="login">
      <h1>Sign In</h1>

      {error && <Message variant="error">{error}</Message>}
      {loading && <Loader />}

      <form
        className="login__form"
        noValidate
        autoComplete="off"
        onSubmit={submitHandler}
      >
        <TextField
          id="name"
          label="Name"
          variant="outlined"
          type="text"
          placeholder="Enter name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <TextField
          id="pass"
          label="Password"
          variant="outlined"
          type="password"
          placeholder="Enter password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button variant="contained" type="submit">
          Login
        </Button>
      </form>
    </div>
  );
};

export default LoginView;
