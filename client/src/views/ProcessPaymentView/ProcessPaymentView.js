import React, { useEffect, useState } from "react";
import "./ProcessPaymentView.scss";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import {
  getOrderDetails,
  deliverOrder,
  payOrder
} from "../../actions/orderActions";
import { payWithStripe } from "../../actions/stripeActions";
import {
  Button,
  List,
  ListItem,
  Grid,
  Paper,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Divider
} from "@material-ui/core";

const ProcessPaymentView = ({ match }) => {
  const [isSubmitted, setIsSubmitted] = useState(false);

  const orderId = match.params.id;
  let search = window.location.search;
  let params = new URLSearchParams(search);
  let isSuccess =
    params.get("purchaseResult") === "success"
      ? true
      : params.get("purchaseResult") === "failed"
      ? false
      : null;

  const dispatch = useDispatch();

  const orderDetails = useSelector((state) => state.orderDetails);
  const { order, loading, error } = orderDetails;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const orderPay = useSelector((state) => state.orderPay);
  const { success: successPay, loading: loadingPay } = orderPay;

  const orderDeliver = useSelector((state) => state.orderDeliver);
  const { success: successDeliver, loading: loadingDeliver } = orderDeliver;

  if (!loading && order) {
    //   Calculate prices
    const addDecimals = (num) => {
      return (Math.round(num * 100) / 100).toFixed(2);
    };

    order.heliumPrice = addDecimals(
      order.orderItems.reduce((acc, item) => {
        return acc + item.heliumPrice * item.qty;
      }, 0)
    );

    order.weightPrice = addDecimals(
      order.orderItems.reduce((acc, item) => {
        return acc + item.weightPrice;
      }, 0)
    );

    order.itemsPrice = addDecimals(
      order.orderItems.reduce((acc, item) => acc + item.price * item.qty, 0)
    );
  }

  useEffect(() => {
    dispatch(getOrderDetails(orderId));
  }, [dispatch, orderId, successPay, successDeliver]);

  const paymentHandler = () => {
    setIsSubmitted(true);

    const { paymentMethod } = order;
    const callbackUrl = buildCallbackUrl();

    switch (paymentMethod.value) {
      case "stripe":
        dispatch(payWithStripe(order._id, callbackUrl));
        break;
      case "cash":
        break;
      case "paypal":
        break;
      default:
        break;
    }
  };

  const isPaidHandler = () => {
    dispatch(payOrder(order._id));
  };

  const deliverHandler = () => {
    dispatch(deliverOrder(order));
  };

  return loading ? (
    <Loader />
  ) : error ? (
    <Message variant="error">{error}</Message>
  ) : (
    <>
      {isSuccess ? (
        <Message variant="info">Maksājums veikts veiksmīgi!</Message>
      ) : (
        isSuccess !== null && (
          <Message variant="error">Maksājums netikai veikts veiksmīgi</Message>
        )
      )}
      <h1>Pasūtijūms {order._id}</h1>
      <Grid container spacing={5}>
        <Grid item xs={12} md={8}>
          <List variant="flush">
            <ListItem>
              <ListItemText
                primary="Pasūtītājs"
                secondary={order.shippingAddress.fullName}
              />
            </ListItem>
            <ListItem>
              <ListItemText
                primary="Telefons"
                secondary={order.shippingAddress.phone}
              />
            </ListItem>
            <ListItem>
              <ListItemText
                primary="Telefons"
                secondary={order.shippingAddress.email}
              />
            </ListItem>
            <ListItem>
              <ListItemText
                primary="Adrese"
                secondary={`${order.shippingAddress.address}, ${order.shippingAddress.city},
                    LV-${order.shippingAddress.postalCode}`}
              />
              {order.isDelivered ? (
                <Message variant="success">
                  Piegādāts: {order.deliveredAt}
                </Message>
              ) : (
                <Message variant="error">Nav Piegādāts</Message>
              )}
            </ListItem>

            <ListItem>
              <ListItemText
                primary="Maksājuma metode"
                secondary={order.paymentMethod.title}
              />

              {order.isPaid ? (
                <Message variant="success">Samaksāts: {order.paidAt}</Message>
              ) : (
                <Message variant="error">Nav samaksāts</Message>
              )}
            </ListItem>

            <ListItem>
              <ListItemText
                primary="Komentāri"
                secondary={order.shippingAddress.comments}
              />
            </ListItem>

            <List>
              {order.orderItems.length === 0 ? (
                <ListItemText primary="Nav preču" />
              ) : (
                <>
                  {order.orderItems.map((item, index) => (
                    <div key={index}>
                      <ListItem>
                        <ListItemAvatar>
                          <Avatar src={item.image[0]} alt={item.name} />
                        </ListItemAvatar>
                        <ListItemText
                          primary="Cena"
                          secondary={`${item.qty} x €${item.price} = €${
                            item.qty * item.price
                          }`}
                        />
                        {item.qty} x €{item.price} = €{item.qty * item.price}
                      </ListItem>
                      <Link to={`/product/${item.product}`}>{item.name}</Link>
                      <Divider />
                    </div>
                  ))}
                </>
              )}
            </List>
          </List>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper>
            <List variant="flush">
              <ListItem>
                <h2>Pasūtijūma kopsavilkums</h2>
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="Preces"
                  secondary={`€${order.itemsPrice}`}
                />
              </ListItem>
              {Number(order.heliumPrice) + Number(order.weightPrice) > 0 && (
                <ListItem>
                  <ListItemText
                    primary="Papildus (Hēlijs, atsvari)"
                    secondary={`€${
                      Number(order.heliumPrice) + Number(order.weightPrice)
                    }`}
                  />
                </ListItem>
              )}

              {order.heliumPrice + order.weightPrice > 0 && (
                <ListItem>
                  <ListItemText
                    primary="Papildus"
                    secondary={`€${order.heliumPrice + order.weightPrice}`}
                  />
                </ListItem>
              )}

              <ListItem>
                <ListItemText
                  primary="Piegāde"
                  secondary={`€${order.shippingPrice}`}
                />
              </ListItem>
              <ListItem>
                <ListItemText primary="PVN" secondary={`€${order.taxPrice}`} />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="Pirms PVN"
                  secondary={`€${
                    Number(order.totalPrice) - Number(order.taxPrice)
                  }`}
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  primary="Kopā"
                  secondary={`€${order.totalPrice}`}
                />
              </ListItem>
              {!order.isPaid &&
                order.paymentMethod.value === "stripe" &&
                !isSuccess && (
                  <ListItem>
                    {loadingPay && <Loader />}
                    <Button
                      onClick={paymentHandler}
                      variant="contained"
                      color="primary"
                      disabled={isSubmitted}
                    >
                      Veikt apmaksu
                    </Button>
                  </ListItem>
                )}
              {userInfo && userInfo.isAdmin && !order.isPaid && (
                <ListItem>
                  <Button
                    type="button"
                    variant="contained"
                    color="secondary"
                    onClick={isPaidHandler}
                  >
                    Atzīmēt kā apmaksātu
                  </Button>
                </ListItem>
              )}

              {loadingDeliver && <Loader />}
              {userInfo &&
                userInfo.isAdmin &&
                order.isPaid &&
                !order.isDelivered && (
                  <ListItem>
                    <Button
                      type="button"
                      variant="contained"
                      color="primary"
                      onClick={deliverHandler}
                    >
                      Atzīmēt kā piegādātu
                    </Button>
                  </ListItem>
                )}
            </List>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

const buildCallbackUrl = () => {
  const protocol = window.location.protocol,
    hostname = window.location.hostname,
    port = window.location.port;

  let callbackUrl = `${protocol}//${hostname}`;

  if (port) {
    callbackUrl += `:${port}`;
  }

  callbackUrl += "/order";

  return callbackUrl;
};

export default ProcessPaymentView;
