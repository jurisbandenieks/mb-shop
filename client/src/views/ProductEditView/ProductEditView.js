import React, { useState, useEffect } from "react";
import "./ProductEditView.scss";
import axios from "axios";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import {
  listProductDetails,
  updateProduct
} from "../../actions/productActions";
import { PRODUCT_UPDATE_RESET } from "../../constants/productConstants";
import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Select,
  TextField
} from "@material-ui/core";

const ProductEditView = ({ match, history }) => {
  const id = match.params.id;

  const [name, setName] = useState("");
  const [productId, setProductId] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState([""]);
  const [category, setCategory] = useState({ title: "", value: "" });
  const [countInStock, setCountInStock] = useState(0);
  const [description, setDescription] = useState("");
  const [uploading, setUploading] = useState(false);
  const [canDeliver, setCanDeliver] = useState(false);
  const [heliumPrice, setHeliumPrice] = useState(0);
  const [weightPrice, setWeightPrice] = useState(0);
  const [canInflate, setCanInflate] = useState(true);
  const [categoryListItems, setCategoryListItems] = useState([]);

  const dispatch = useDispatch();

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  const categoryList = useSelector((state) => state.categoryList);
  const { categories } = categoryList;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate
  } = productUpdate;

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      history.push("/admin/productlist");
    } else {
      if (!product.name || product._id !== id) {
        dispatch(listProductDetails(id));
      } else {
        setCategoryListItems(categories || []);

        setName(product.name || "");
        setProductId(product.productId || "");
        setPrice(product.price || 0);
        setImage(product.image || [""]);
        setCategory(product.category || { title: "", value: "" });
        setCountInStock(product.countInStock || 0);
        setDescription(product.description || "");
        setCanDeliver(product.canDeliver || false);
        setCanInflate(product.canInflate || false);
        setHeliumPrice(product.heliumPrice || 0);
        setWeightPrice(product.weightPrice || 0);
      }
    }
  }, [product, id, dispatch, successUpdate, history, categories]);

  const handleImageChange = (value, index) => {
    let changed = [...image];
    changed[index] = value;

    setImage(changed);
  };

  // const deleteImageHandler = async (e) => {};

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append("image", file);
    setUploading(true);

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
          "Content-Type": "multipart/form-data"
        }
      };

      const { data } = await axios.post("/api/upload", formData, config);

      let changed = [...image];
      changed.push(data);

      setImage(changed);
      setUploading(false);
    } catch (error) {
      console.error(error);
      setUploading(false);
    }
  };

  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(
      updateProduct({
        _id: id,
        productId,
        name,
        price,
        image: image.filter((el) => el),
        category,
        countInStock,
        canDeliver,
        canInflate,
        heliumPrice,
        weightPrice,
        description
      })
    );
  };

  return (
    <>
      <Button
        component={Link}
        to="/admin/productlist"
        color="primary"
        variant="contained"
      >
        Go Back
      </Button>

      <h1>Edit Product</h1>
      {loadingUpdate && <Loader />}
      {errorUpdate && <Message variant="error">{errorUpdate}</Message>}
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="error">{error}</Message>
      ) : (
        <form onSubmit={submitHandler} className="product-form">
          <TextField
            id="productId"
            label="productId"
            type="text"
            value={productId}
            onChange={(e) => setProductId(e.target.value)}
            required
          />

          <TextField
            id="name"
            label="Name"
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />

          <TextField
            id="price"
            label="Price"
            type="number"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />

          {image.map((el, index) => (
            <TextField
              key={index}
              id="image"
              label="Image"
              type="text"
              value={el}
              onChange={(e) => handleImageChange(e.target.value, index)}
            />
          ))}

          <label htmlFor="upload-image">
            <input
              style={{ display: "none" }}
              id="upload-image"
              name="upload-image"
              type="file"
              onChange={uploadFileHandler}
            />

            <Button color="secondary" variant="contained" component="span">
              Upload image
            </Button>
          </label>

          {uploading && <Loader />}

          <TextField
            id="countInStock"
            label="Count in stock"
            type="number"
            value={countInStock}
            onChange={(e) => setCountInStock(e.target.value)}
            required
          />

          <FormControl>
            <InputLabel htmlFor="category-label">Category</InputLabel>
            <Select
              labelId="category-label"
              id="category-select"
              value={category.value}
              required
            >
              <MenuItem value="">
                <em>Select option...</em>
              </MenuItem>
              {categoryListItems.map(
                (category) =>
                  category &&
                  category.subCategories &&
                  category.subCategories.length > 0 &&
                  category.subCategories.map((subCategory) => (
                    <MenuItem
                      key={subCategory._id}
                      value={subCategory.value}
                      onClick={() => setCategory(subCategory)}
                    >
                      {category.name} {">"} {subCategory.name}
                    </MenuItem>
                  ))
              )}
            </Select>
          </FormControl>

          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={() => setCanInflate(!canInflate)}
              />
            }
            checked={canInflate}
            readOnly
            label="Can Inflate"
            labelPlacement="end"
          />

          {canInflate && (
            <TextField
              id="Helium"
              label="Helium price"
              type="number"
              value={heliumPrice}
              onChange={(e) => setHeliumPrice(e.target.value)}
            />
          )}

          {canInflate && (
            <TextField
              id="Weight"
              label="Weight price"
              type="number"
              value={weightPrice}
              onChange={(e) => setWeightPrice(e.target.value)}
            />
          )}

          <TextField
            id="Description"
            label="Write description"
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />

          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                onChange={() => setCanDeliver(!canDeliver)}
              />
            }
            checked={canDeliver}
            readOnly
            label="Can deliver"
            labelPlacement="end"
          />

          <Button type="submit" variant="contained" color="primary">
            Update
          </Button>
        </form>
      )}
    </>
  );
};

export default ProductEditView;
