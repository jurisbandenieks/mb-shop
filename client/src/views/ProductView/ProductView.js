import React, { useEffect, useState } from "react";
import "./ProductView.scss";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { listProductDetails } from "../../actions/productActions";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import Meta from "../../components/Meta";

import {
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  List,
  ListItem,
  Button,
  Paper,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";

const ProductView = ({ match, history }) => {
  const [qty, setQty] = useState(1);
  const [needHelium, setNeedHelium] = useState(false);
  const [needWeight, setNeedWeight] = useState(false);

  const dispatch = useDispatch();

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  useEffect(() => {
    dispatch(listProductDetails(match.params.id));
  }, [dispatch, match]);

  const addToCartHandler = () => {
    const helium = needHelium ? 1 : 0;
    const weight = needWeight ? 1 : 0;

    history.push(
      `/cart/${match.params.id}?qty=${qty}?helium=${helium}?weight=${weight}`
    );
  };

  return (
    <>
      <Meta title={product.name} />
      <Button variant="contained" color="primary" component={Link} to="/">
        Atpakaļ
      </Button>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="error"> {error}</Message>
      ) : (
        <div className="mt-2">
          <Meta title={product.name} />
          <Grid container spacing={5}>
            <Grid item xs={12} md={3}>
              <img
                src={product.image && product.image[0]}
                alt={product.name}
                style={{ width: "100%" }}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <List>
                <ListItem>
                  <h3>{product.name}</h3>
                </ListItem>
                <ListItem>Cena: €{product.price}</ListItem>
                <ListItem>Apraksts: {product.description}</ListItem>
              </List>
            </Grid>
            <Grid item xs={12} md={3}>
              <Paper>
                <List>
                  <ListItem>
                    <Grid container spacing={2}>
                      <Grid item xs={12} md={6}>
                        Cena:
                      </Grid>
                      <Grid item xs={12} md={6}>
                        <strong>{product.price}</strong>
                      </Grid>
                    </Grid>
                  </ListItem>
                  <ListItem>
                    <Grid container spacing={2}>
                      <Grid item xs={12} md={6}>
                        Status:
                      </Grid>
                      <Grid item xs={12} md={6}>
                        {product.countInStock > 0 ? "Pieejams" : "Nav pieejams"}
                      </Grid>
                    </Grid>
                  </ListItem>
                  {product.canInflate && product.heliumPrice > 0 && (
                    <ListItem>
                      <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                          Hēlijs
                        </Grid>
                        <Grid item xs={12} md={6}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                color="primary"
                                onChange={() => setNeedHelium(!needHelium)}
                              />
                            }
                            checked={needHelium}
                            readOnly
                            label={"€" + product.heliumPrice.toFixed(2)}
                            labelPlacement="end"
                          />
                        </Grid>
                      </Grid>
                    </ListItem>
                  )}
                  {product.canInflate && product.weightPrice > 0 && (
                    <ListItem>
                      <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                          Atsvari
                        </Grid>
                        <Grid item xs={12} md={6}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                color="primary"
                                onChange={() => setNeedWeight(!needWeight)}
                              />
                            }
                            checked={needWeight}
                            readOnly
                            label={"€" + product.weightPrice.toFixed(2)}
                            labelPlacement="end"
                          />
                        </Grid>
                      </Grid>
                    </ListItem>
                  )}

                  {product.countInStock > 0 && (
                    <ListItem>
                      <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                          Daudzums
                        </Grid>
                        <Grid item xs={12} md={6}>
                          <FormControl>
                            <InputLabel htmlFor="qty-label">
                              Daudzums
                            </InputLabel>
                            <Select
                              labelId="qty-label"
                              id="qty-select"
                              value={qty}
                              required
                              onChange={(e) => setQty(e.target.value)}
                            >
                              {[...Array(product.countInStock).keys()].map(
                                (x) => (
                                  <MenuItem key={x + 1} value={x + 1}>
                                    {x + 1}
                                  </MenuItem>
                                )
                              )}
                            </Select>
                          </FormControl>
                        </Grid>
                      </Grid>
                    </ListItem>
                  )}

                  <ListItem>
                    <Button
                      onClick={addToCartHandler}
                      className="btn-block"
                      variant="outlined"
                      type="button"
                      disabled={product.countInStock === 0}
                    >
                      Pievienot grozam
                    </Button>
                  </ListItem>
                </List>
              </Paper>
            </Grid>
          </Grid>
        </div>
      )}
    </>
  );
};

export default ProductView;
