import React, { useState, useEffect } from "react";
import "./CategoryListView.scss";

import { useDispatch, useSelector } from "react-redux";
import {
  List,
  ListItemText,
  ListItem,
  IconButton,
  ListItemSecondaryAction,
  Grid,
  TextField,
  Button
} from "@material-ui/core";
import {
  createCategory,
  deleteCategory,
  listCategories,
  updateCategory
} from "../../actions/categoryActions";
import EditIcon from "@material-ui/icons/Edit";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import DeleteIcon from "@material-ui/icons/Delete";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";

const CategoryListView = ({ history }) => {
  const [categoryItem, setCategoryItem] = useState({});

  const dispatch = useDispatch();

  const categoryList = useSelector((state) => state.categoryList);
  const { loading, error, categories } = categoryList;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    if (userInfo) {
      dispatch(listCategories());
    } else {
      history.push("/login");
    }
  }, [dispatch, history, userInfo]);

  const addCategoryHandler = () => {
    dispatch(createCategory());
  };

  const deleteHandler = (id) => {
    dispatch(deleteCategory(id));
  };

  const updateHandler = (e) => {
    e.preventDefault();

    dispatch(updateCategory(categoryItem));
  };

  const updateValueChanged = (index) => (e) => {
    let newArr = [...categoryItem.subCategories];
    newArr[index].value = e.target.value;

    setCategoryItem({ ...categoryItem, subCategories: newArr });
  };

  const updateNameChanged = (index) => (e) => {
    let newArr = [...categoryItem.subCategories];
    newArr[index].name = e.target.value;

    setCategoryItem({ ...categoryItem, subCategories: newArr });
  };

  const addSubcategory = () => {
    let newArr = [...categoryItem.subCategories];
    newArr.push({ value: "Subvalue", name: "New subcategory" });

    setCategoryItem({ ...categoryItem, subCategories: newArr });
  };

  const removeSubcategory = (i) => {
    let newArr = [...categoryItem.subCategories];
    newArr.splice(i, 1);

    setCategoryItem({ ...categoryItem, subCategories: newArr });
  };

  const renderCategoryItem = (cat) => {
    if (cat.name) {
      return (
        <ListItem key={cat._id}>
          <ListItemText
            primary={cat.name}
            secondary={
              cat.subCategories?.length > 0
                ? `${cat.subCategories.map((x) => x.name).join(", ")}`
                : null
            }
          />

          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="edit"
              onClick={() => setCategoryItem(cat)}
            >
              <EditIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      );
    }
  };

  return (
    <>
      {loading && <Loader />}
      {error && <Message variant="error">{error}</Message>}
      <Grid container spacing={5}>
        <Grid item xs={6}>
          <Button color="primary" onClick={addCategoryHandler}>
            ADD CATEGORY
          </Button>
          <List dense={true}>
            {categories && categories.map((cat) => renderCategoryItem(cat))}
          </List>
        </Grid>
        <Grid item xs={6}>
          <h2>Update category</h2>
          {categoryItem.name && (
            <form onSubmit={updateHandler} className="category__form">
              <TextField
                placeholder="Value"
                label="Value"
                id={categoryItem.value}
                name="value"
                type="text"
                value={categoryItem.value}
                onChange={(e) =>
                  setCategoryItem({ ...categoryItem, value: e.target.value })
                }
              />

              <TextField
                placeholder="Category"
                label="Kategorija"
                id={categoryItem.name}
                name="category"
                type="text"
                value={categoryItem.name}
                onChange={(e) =>
                  setCategoryItem({ ...categoryItem, name: e.target.value })
                }
              />

              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <p>Subcategories</p>
                <IconButton
                  type="button"
                  variant="contained"
                  color="primary"
                  onClick={addSubcategory}
                >
                  <AddCircleOutlineIcon />
                </IconButton>
              </div>

              {categoryItem.subCategories?.length > 0 &&
                categoryItem.subCategories.map((sub, i) => (
                  <div key={i}>
                    <TextField
                      placeholder="Subvalue"
                      label="Value"
                      id={`category-${sub.value}`}
                      name={`category-${sub.value}`}
                      type="text"
                      value={sub.value}
                      onChange={updateValueChanged(i)}
                    />

                    <TextField
                      placeholder="Subcategory"
                      label="Apakškategorija"
                      id={`category-${sub.name}`}
                      name={`category-${sub.name}`}
                      type="text"
                      value={sub.name}
                      onChange={updateNameChanged(i)}
                    />

                    <IconButton
                      type="button"
                      variant="contained"
                      color="primary"
                      onClick={() => removeSubcategory(i)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                ))}

              <Button
                type="button"
                variant="contained"
                color="secondary"
                disabled={!categoryItem._id}
                onClick={() => deleteHandler(categoryItem._id)}
              >
                DELETE
              </Button>
              <Button type="submit" variant="contained" color="primary">
                OK
              </Button>
            </form>
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default CategoryListView;
