import axios from "axios";
import {
  STRIPE_PAY_FAIL,
  STRIPE_PAY_SUCCESS,
  STRIPE_PAY_REQUEST
} from "../constants/stripeConstants";
import { logout } from "./userActions";
import { loadStripe } from "@stripe/stripe-js";

export const payWithStripe = (orderId, callbackUrl) => async (dispatch) => {
  try {
    dispatch({
      type: STRIPE_PAY_REQUEST
    });

    const config = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    const { data } = await axios.post(
      `/api/payments/pay-stripe`,
      { orderId, callbackUrl },
      config
    );

    const stripe = await loadStripe(data.stripePublicKey);

    stripe.redirectToCheckout({
      sessionId: data.stripeCheckoutSessionId
    });

    dispatch({
      type: STRIPE_PAY_SUCCESS
    });
  } catch (error) {
    const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message;
    if (message === "Not authorized, token failed") {
      dispatch(logout());
    }
    dispatch({
      type: STRIPE_PAY_FAIL,
      payload: message
    });
  }
};
