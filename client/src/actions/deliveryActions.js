import axios from "axios";
import {
  DELIVERY_LIST_REQUEST,
  DELIVERY_LIST_SUCCESS,
  DELIVERY_LIST_FAIL
} from "../constants/deliveryConstants";

export const listDeliveryMethods = () => async (dispatch) => {
  try {
    dispatch({ type: DELIVERY_LIST_REQUEST });

    const { data } = await axios.get(`/api/deliveries`);

    dispatch({
      type: DELIVERY_LIST_SUCCESS,
      payload: data
    });
  } catch (error) {
    dispatch({
      type: DELIVERY_LIST_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
    });
  }
};
