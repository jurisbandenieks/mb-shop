import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { cartReducer } from "./reducers/cartReducers";
import {
  categoryListReducer,
  categoryDeleteReducer,
  categoryCreateReducer,
  categoryUpdateReducer
} from "./reducers/categoryReducers";
import {
  productListReducer,
  productDetailsReducer,
  productDeleteReducer,
  productCreateReducer,
  productUpdateReducer
} from "./reducers/productReducers";
import { userLoginReducer } from "./reducers/userReducers";
import {
  orderCreateReducer,
  orderDetailsReducer,
  orderPayReducer,
  orderDeliverReducer,
  orderListReducer
} from "./reducers/orderReducers";
import { deliveryListReducer } from "./reducers/deliveryReducers";
import { getCookie } from "./utils/cookieUtils";

const reducer = combineReducers({
  cart: cartReducer,

  productList: productListReducer,
  productDetails: productDetailsReducer,
  productDelete: productDeleteReducer,
  productCreate: productCreateReducer,
  productUpdate: productUpdateReducer,

  categoryList: categoryListReducer,
  categoryDelete: categoryDeleteReducer,
  categoryCreate: categoryCreateReducer,
  categoryUpdate: categoryUpdateReducer,

  userLogin: userLoginReducer,
  // userRegister: userRegisterReducer,

  orderCreate: orderCreateReducer,
  orderDetails: orderDetailsReducer,
  orderPay: orderPayReducer,
  orderDeliver: orderDeliverReducer,
  orderList: orderListReducer,

  // stripePay: stripePayReducer,

  deliveryList: deliveryListReducer
});

const carItemsFromStorage = getCookie("cartItems")
  ? JSON.parse(getCookie("cartItems"))
  : [];

const userInfoFromStorage = getCookie("userInfo")
  ? JSON.parse(getCookie("userInfo"))
  : null;

const shippingAddressFromStorage = getCookie("shippingAddress")
  ? JSON.parse(getCookie("shippingAddress"))
  : {};

const paymentMethodFromStorage = getCookie("paymentMethod")
  ? JSON.parse(getCookie("paymentMethod"))
  : {};

const initialState = {
  cart: {
    cartItems: carItemsFromStorage,
    shippingAddress: shippingAddressFromStorage,
    paymentMethod: paymentMethodFromStorage
  },
  userLogin: { userInfo: userInfoFromStorage }
};

const middleWare = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleWare))
);

export default store;
