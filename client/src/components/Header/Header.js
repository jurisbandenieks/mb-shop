import React, { useState, useEffect } from "react";
import "./Header.scss";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  Button,
  Container,
  Drawer,
  List,
  ListItemText,
  ListItem,
  Menu,
  MenuItem
} from "@material-ui/core";

import Loader from "../Loader/Loader";
import Message from "../Message/Message";
import { listCategories } from "../../actions/categoryActions";
import { logout } from "../../actions/userActions";

const Header = () => {
  const [left, setLeft] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const dispatch = useDispatch();

  const categoryList = useSelector((state) => state.categoryList);
  const { loading, error, categories } = categoryList;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    dispatch(listCategories());
  }, [dispatch]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logoutHandler = () => {
    dispatch(logout());
  };

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setLeft(open);
  };

  const adminSection = () => {
    return (
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        color="primary"
        onClick={handleClick}
      >
        Admin
      </Button>
    );
  };

  const logoutSection = () => {
    return (
      <Button color="secondary" onClick={logoutHandler}>
        Logout
      </Button>
    );
  };

  return (
    <>
      <header className="header">
        <Link to={"/"}>
          <img
            src="/images/logo.png"
            alt="Mārtiņa Ballīte"
            className="header__logo"
          />
        </Link>
        <Container style={{ height: "100%" }}>
          <nav className="header__nav">
            <Button onClick={toggleDrawer(true)}>Kategorijas</Button>
            <Button component={Link} to="/cart">
              Grozs
            </Button>

            {userInfo && adminSection()}

            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>
                <Link to={"/admin/productlist"}>Produkti</Link>
              </MenuItem>
              <MenuItem onClick={handleClose}>
                <Link to={"/admin/categorylist"}>Kategorijas</Link>
              </MenuItem>
              <MenuItem onClick={handleClose}>
                <Link to={"/admin/orderlist"}>Pasūtījumi</Link>
              </MenuItem>
            </Menu>
            {userInfo && logoutSection()}
          </nav>
        </Container>
      </header>
      <Drawer anchor="left" open={left} onClose={toggleDrawer(false)}>
        <div className="drawer">
          {loading ? (
            <Loader />
          ) : error ? (
            <Message variant="error">{error}</Message>
          ) : (
            <List dense={true}>
              {categories.map((category) => (
                <div key={category._id}>
                  <ListItem divider>
                    <ListItemText primary={category.name} />
                  </ListItem>
                  <List dense={true} component="div" disablePadding>
                    {category.subCategories?.length > 0 &&
                      category.subCategories.map((child, index) => (
                        <ListItem
                          key={index}
                          button
                          divider
                          component={Link}
                          to={`/search/${child.value}`}
                          onClick={toggleDrawer(false)}
                        >
                          <ListItemText
                            primary={child.name}
                            className="drawer__sub"
                          />
                        </ListItem>
                      ))}
                  </List>
                </div>
              ))}
            </List>
          )}
        </div>
      </Drawer>
    </>
  );
};

export default Header;
