import React from "react";
import CookieConsent from "react-cookie-consent";

const Cookie = () => {
  return (
    <CookieConsent
      buttonText="Piekrītu"
      style={{
        background: " #883997",
        color: "#fafafa"
      }}
      buttonStyle={{ color: "#000", background: "#4fc3f7" }}
    >
      Šī mājaslapa izmanto Cookies.
    </CookieConsent>
  );
};

export default Cookie;
