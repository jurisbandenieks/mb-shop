import React from "react";
import { Helmet } from "react-helmet";

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
    </Helmet>
  );
};

Meta.defaultProps = {
  title: "Laipni lūdzam Mārtiņa Ballītē",
  keywords: "Baloni, Svētku piederumi, dekorācijas, Svētki",
  description:
    "Interneta veikals svētku piederumiem - baloni, iesaiņojumi, galda piederumi un vēstules"
};

export default Meta;
