import React from "react";
import "./Product.scss";

import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";

const Product = ({ history, product }) => {
  const qty = 1;

  const addToCartHandler = () => {
    history.push(`/cart/${product._id}?qty=${qty}?helium=${0}?weight=${0}`);
  };

  return (
    <Card className="product">
      <Link to={`/products/${product._id}`} className="product__content">
        <CardActionArea className="product__area">
          <CardMedia
            component="img"
            height="150"
            alt={product.name}
            image={product.image[0]}
            title={product.name}
            className="product__media"
          />
          <CardContent className="product__text">
            <Typography className="product__title">{product.name}</Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className="product__desc"
            >
              {product.description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
      <CardActions className="product__actions">
        <Button
          color="primary"
          variant="outlined"
          style={{ width: "100%" }}
          onClick={addToCartHandler}
          disabled={!product.countInStock}
        >
          {product.countInStock
            ? `€${product.price.toFixed(2)}`
            : "Nav pieejams"}
        </Button>
      </CardActions>
    </Card>
  );
};

export default Product;
