import {
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import React from "react";
import "./PrivacyContent.scss";

const PrivacyContent = ({ handleDisagree, handleAgree }) => {
  return (
    <>
      <DialogTitle id="alert-dialog-title">{"Privātuma politika"}</DialogTitle>
      <DialogContent>
        <DialogContentText
          id="alert-dialog-description"
          component={"span"}
          variant={"body2"}
        >
          <div>
            <p>
              Šī privātuma politika informē par privātuma praksi un personas
              datu apstrādes principiem saistībā ar SIA Mazliet vairāk gaisa
              mājas lapu un pakalpojumiem. Lai sazinātos saistībā ar datu
              apstrādes jautājumiem, lūdzu rakstiet e-pastu uz
              martina.ballite@gmail.com.
            </p>
            <br />
            <h2>Kādu informāciju mēs iegūstam?</h2>
            <p>
              Mēs iegūstam tādus personas datus, ko jūs mums brīvprātīgi
              sniedzat ar e-pasta starpniecību, aizpildot tīmeklī bāzētās
              anketas un citā tiešā saziņā ar jums. Iesniedzot pasūtījumu, jums
              jānorāda vārds, uzvārds, kontaktinformācija, piegādes adrese, un
              cita informācija kuru jūs vēlaties sniegt.
            </p>
            <br />
            <h2>Kā mēs izmantojam iegūtos personas datus?</h2>
            <p>Mēs varam izmantot iegūtos personas datus, lai</p>
            <ul>
              <li>
                • sniegtu jums jūsu pieprasītos pakalpojumus un informāciju,
              </li>
              <li>
                • apstrādātu jūsu pasūtījumus un noformētu nepieciešamos
                dokumentus,
              </li>
              <li>• sniegtu jums efektīvu klientu atbalstu,</li>
              <li>
                • palīdzētu novērst apdraudējumu vai krāpnieciskas darbības,
              </li>
              <li>
                • nosūtītu jums informatīvus ziņojumus, ja esat nepārprotami
                piekrituši tādus saņemt,
              </li>
              <li>• ievērotu normatīvo aktu prasības,</li>
            </ul>
            <br />
            <p>
              Mēs varam nodot jūsu informāciju trešajām personām, lai ievērotu
              normatīvo aktu prasības, sadarbotos ar uzraudzības iestādēm,
              palīdzētu novērstu noziedzīgas darbības un aizsargātu mūsu, jūsu
              un citu personu likumīgās tiesības.
            </p>
            <br />
            <h2>Kā mēs aizsargājam personas datus?</h2>
            <p>
              Jūsu personas datu aizsardzībai mēs izmantojam dažādus tehniskus
              un organizatoriskus drošības pasākumus. Jūsu personas dati ir
              pieejami ierobežotam cilvēku skaitam, tikai pilnvarotām personām.
            </p>
            <br />
            <h2>Cik ilgi mēs glabājam personas datus?</h2>
            <p>
              Mēs glabājam jūsu personas datus tik ilgi, cik ilgi tie ir mums
              nepieciešami atbilstoši to ieguves mērķim un kā to pieļauj vai
              nosaka normatīvo aktu prasības.
            </p>
            <br />
            <h2>Kā mēs izmantojam sīkdatnes?</h2>
            <p>
              Sīkdatnes ir nelielas teksta datnes, ko jūsu apmeklētās mājas
              lapas saglabā jūsu datorā. Tās tiek izmantotas, lai nodrošinātu
              mājas lapas darbību, kā arī lai sniegtu informāciju vietnes
              īpašniekiem.
            </p>
            <br />
            <h2>Kā atteikties no sīkdatnēm?</h2>
            <p>
              Lai atteiktos no sīkdatņu saņemšanas, jūs varat izmantojot
              privātās pārlūkošanas režīmu, kuru nodrošina lielākā daļa
              pārlūkprogrammu (privāts logs, inkognito logs vai InPrivate logs).
              Jebkādas sīkdatnes, kas tiek izveidotas, darbojoties privātās
              pārlūkošanas režīmā, tiek dzēstas, tiklīdz jūs aizverat visus
              pārlūka logus.
            </p>
            <br />
            <h2>Jūsu tiesības saistībā ar jūsu personas datiem</h2>
            <p>
              Ja jūs esat datu subjekts saskaņā ar ES VDAR (piemēram, jūs esat
              ES pilsonis un sniedzat mums savus personas datus), jums ir
              turpmāk minētās tiesības saistībā ar saviem personas datiem:
            </p>
            <ul>
              <li>
                • Tiesības piekļūt informācijai. Jums ir tiesības saņemt
                informāciju par to, kāpēc un kā tiek apstrādāti jūsu personas
                dati. Jums ir tiesības bez maksas saņemt mūsu rīcībā esošo jūsu
                personas datu kopiju plaši izmantotā elektroniskā formātā.
              </li>
              <li>
                • Tiesības labot. Jums ir tiesības panākt neprecīzu vai
                nepilnīgu personas datu labošanu vai papildināšanu bez
                nepamatotas kavēšanās.
              </li>
              <li>
                • Tiesības „tikt aizmirstam”. Jums ir tiesības atsaukt savu
                piekrišanu personas datu apstrādei un panākt savu personas datu
                dzēšanu bez nepamatotas kavēšanās, tiklīdz dati vairs nav
                nepieciešami, lai sniegtu jūsu pieprasītos pakalpojumus un
                ievērotu normatīvo aktu prasības.
              </li>
              <li>
                • Tiesības ierobežot apstrādi. Jums ir tiesības panākt savu
                personas datu apstrādes ierobežošanu, ja jūs iebilstat pret to
                un mums nav leģitīmu pamatu turpināt apstrādi, ja jūs apstrīdat
                datu precizitāti, ja apstrāde ir pretlikumīga vai ja jūs
                pieprasāt celt, īstenot vai aizstāvēt likumīgas prasības.
              </li>
              <li>
                • Tiesības iebilst. Jums ir tiesības jebkurā brīdī iebilst pret
                datu apstrādi, ja vien tas nav nepieciešams sabiedrības
                interesēs veicamam uzdevumam vai apstrādei nepastāv neapstrīdami
                likumīgs pamats.
              </li>
              <li>
                • Citas tiesības saskaņā ar VDAR. Vairāk informācijas skatiet,
                apmeklējot ES datu aizsardzībai veltīto mājas lapu.
              </li>
            </ul>
          </div>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={handleDisagree}>
          Nepiekrītu
        </Button>
        <Button color="primary" onClick={handleAgree} autoFocus>
          Piekrītu
        </Button>
      </DialogActions>
    </>
  );
};

export default PrivacyContent;
