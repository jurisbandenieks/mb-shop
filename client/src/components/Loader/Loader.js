import React from "react";
import "./Loader.scss";
import CircularProgress from "@material-ui/core/CircularProgress";

const Loader = () => {
  return <CircularProgress className="loader" size={100} />;
};

export default Loader;
