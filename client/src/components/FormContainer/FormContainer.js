import React from "react";
import { Container, Grid } from "@material-ui/core";

const FormContainer = ({ children }) => {
  return (
    <Container className="form-container">
      <Grid container>
        <Grid item xs={12} md={6}>
          {children}
        </Grid>
      </Grid>
    </Container>
  );
};

export default FormContainer;
