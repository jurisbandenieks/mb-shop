import React from "react";
import "./Footer.scss";

import { Container } from "@material-ui/core";

const Footer = () => {
  return (
    <footer className="footer">
      <Container>
        <div className="footer__container">
          <div className="footer__left">
            <h2>Kontakti</h2>
            <p>SIA Mazliet vairāk gaisa</p>
            <p>20266572</p>
            <p>martina.ballite@gmail.com</p>
            <p>Reģ. Nr. - 53603090431</p>
            <p>Jelgava, Pērnavas iela 12 - 46, LV-3004</p>
            <div className="footer__social">
              <p>Seko mums</p>
              <div>
                <a href="https://www.facebook.com/martinaballite/">
                  <i className="fab fa-facebook"></i>
                </a>
                <a href="https://www.instagram.com/martinaballite/">
                  <i className="fab fa-instagram "></i>
                </a>
              </div>
            </div>
          </div>
          <div className="footer__right">
            <p>
              Created by{" "}
              <a href="https://www.arbor-dev.com">
                <strong>Arbor Dev</strong>
              </a>
            </p>
            <p>bandenieks.juris@gmail.com</p>
            <p>Juris Bandenieks &copy; 2021</p>
          </div>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
