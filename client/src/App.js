import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Container } from "@material-ui/core";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import HomeView from "./views/HomeView/HomeView";
import ProductView from "./views/ProductView/ProductView";
import LoginView from "./views/LoginView/LoginView";
import OrderStepsView from "./views/OrderStepsView/OrderStepsView";
import ProcessPaymentView from "./views/ProcessPaymentView/ProcessPaymentView";
import ProductListView from "./views/ProductListView/ProductListView";
import ProductEditView from "./views/ProductEditView/ProductEditView";
import OrderListView from "./views/OrderListView/OrderListView";
import CategoryListView from "./views/CategoryListView/CategoryListView";
import Cookie from "./components/Cookie";

function App() {
  return (
    <>
      <Router>
        <Header />
        <Container className="main">
          <Route path="/products/:id" component={ProductView} />
          <Route path="/cart/:id?" component={OrderStepsView} />
          <Route path="/order/:id" component={ProcessPaymentView} />
          <Route path="/admin" component={LoginView} exact />

          <Route path="/admin/orderlist" component={OrderListView} exact />
          <Route
            path="/admin/categorylist"
            component={CategoryListView}
            exact
          />

          <Route path="/admin/productlist" component={ProductListView} exact />
          <Route
            path="/admin/productlist/:pageNumber"
            component={ProductListView}
            exact
          />
          <Route path="/admin/product/:id/edit" component={ProductEditView} />

          <Route path="/search/:keyword" component={HomeView} exact />
          <Route path="/page/:pageNumber" component={HomeView} exact />
          <Route
            path="/search/:keyword/page/:pageNumber"
            component={HomeView}
          />
          <Route path="/" component={HomeView} exact />
        </Container>
        <Footer />
      </Router>
      <Cookie />
    </>
  );
}

export default App;
