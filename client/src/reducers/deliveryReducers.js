import {
  DELIVERY_LIST_REQUEST,
  DELIVERY_LIST_SUCCESS,
  DELIVERY_LIST_FAIL
} from "../constants/deliveryConstants";

export const deliveryListReducer = (
  state = { deliveryMethods: [] },
  action
) => {
  switch (action.type) {
    case DELIVERY_LIST_REQUEST:
      return { loading: true, deliveryMethods: [] };
    case DELIVERY_LIST_SUCCESS:
      return {
        loading: false,
        deliveryMethods: action.payload
      };
    case DELIVERY_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
