import {
  STRIPE_PAY_REQUEST,
  STRIPE_PAY_SUCCESS,
  STRIPE_PAY_FAIL,
  STRIPE_PAY_RESET
} from "../constants/stripeConstants";

export const stripePayReducer = (state = {}, action) => {
  switch (action.type) {
    case STRIPE_PAY_REQUEST:
      return {
        loading: true
      };
    case STRIPE_PAY_SUCCESS:
      return {
        loading: false,
        success: true
      };
    case STRIPE_PAY_FAIL:
      return {
        loading: false,
        error: action.payload
      };
    case STRIPE_PAY_RESET:
      return {};
    default:
      return state;
  }
};
